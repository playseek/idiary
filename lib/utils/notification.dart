import 'package:diary/utils/datetime.dart';
import 'package:diary/utils/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

const REMINDER_CHANNEL_ID = '1';
const REMINDER_ID = 1;

class NotificationManager {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  NotificationManager() {
    _init();
  }

  void _init() {
    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: _onDidReceiveLocalNotification);
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: _onSelectNotification);
  }

  Future _onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: ' + payload);
    }
  }

  Future _onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
  }

  void scheduleReminder(String time) async {
    final reminderTime = Time(getHour(time), getMinute(time));
    final androidPlatformChannelSpecifics = AndroidNotificationDetails(
      REMINDER_CHANNEL_ID,
      AppStrings.appName,
      AppStrings.reminderNotificationDescription,
    );
    final iOSPlatformChannelSpecifics = IOSNotificationDetails(
      presentAlert: true,
      presentSound: true,
      presentBadge: true,
    );
    final platformChannelSpecifics = NotificationDetails(
      androidPlatformChannelSpecifics,
      iOSPlatformChannelSpecifics,
    );
    await flutterLocalNotificationsPlugin.showDailyAtTime(
      REMINDER_ID,
      AppStrings.appName,
      AppStrings.reminderNotificationDescription,
      reminderTime,
      platformChannelSpecifics,
    );
  }

  void cancelReminder() {
    flutterLocalNotificationsPlugin.cancel(REMINDER_ID);
  }
}

abstract class AppStrings {
  static const appName = 'iDiary';
  static const newDiaryTooltip = 'New Diary';
  static const newNoteHint = 'Write here...';
  static const save = 'Save';
  static const defaultReminder = '9:00';
  static const reminderDescription = 'Hold to select reminder time';
  static const reminderNotificationDescription =
      'Hey, fill your today\'s diary';

  static String remindAt(String time) => 'Remind at $time';
}

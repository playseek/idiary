import 'package:intl/intl.dart';

String formatDayDate(DateTime date) =>
    date == null ? "" : DateFormat.yMMMd().format(date);

String formatUpdatedAtDate(DateTime date) {
  if (date == null) return "";
  date = date.toLocal();

  var dialogDate = DateTime(date.year, date.month, date.day);
  var now = DateTime.now();
  var current = DateTime(now.year, now.month, now.day);

  if (dialogDate.difference(current).inDays == 0) {
    return DateFormat("HH:mm").format(date);
  } else {
    return DateFormat(" HH:mm, dd MMM yyy").format(date);
  }
}

int getMinute(String time) => int.parse(time.split(':')[1]);

int getHour(String time) => int.parse(time.split(':')[0]);

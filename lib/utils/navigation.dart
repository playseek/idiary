import 'package:diary/presentation/diary/diary.dart';
import 'package:flutter/material.dart';

void openDayPage(BuildContext context, DateTime date) {
  Navigator.push(
    context,
    MaterialPageRoute(
      builder: (context) => DiaryPage(date),
    ),
  );
}

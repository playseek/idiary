import 'package:built_redux/built_redux.dart';
import 'package:diary/data/data.dart';
import 'package:diary/utils/notification.dart';
import 'package:diary/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';

import 'home/home.dart';

class DiaryApp extends StatelessWidget {
  final _store = Store<AppState, AppStateBuilder, AppActions>(
    appReducerBuilder.build(),
    AppState(),
    AppActions(),
    middleware: [
      createAppMiddleware(
        RealAppRepository(
          RealAppLocalDataSource(
            SharedPreferencesManager(),
          ),
          notificationManager: NotificationManager(),
        ),
      ),
    ],
  );
  @override
  Widget build(BuildContext context) {
    return ReduxProvider(
      store: _store,
      child: MaterialApp(
        builder: (context, child) => MediaQuery(
              data:
                  MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
              child: child,
            ),
        title: AppStrings.appName,
        theme: ThemeData.dark(),
        home: HomePage(),
      ),
    );
  }
}

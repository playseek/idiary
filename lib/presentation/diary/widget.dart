import 'package:diary/data/diary/diary.dart';
import 'package:diary/utils/bottom_sheet.dart';
import 'package:diary/utils/datetime.dart';
import 'package:diary/utils/strings.dart';
import 'package:flutter/material.dart';

class DiaryWidget extends StatefulWidget {
  final Day day;
  final Function(String) onCreateNote;
  final Function(UpdateNotePayload) onNoteUpdated;
  final VoidCallback onOpen;
  final VoidCallback onClose;

  const DiaryWidget(
    this.day, {
    Key key,
    this.onCreateNote,
    this.onNoteUpdated,
    this.onOpen,
    this.onClose,
  }) : super(key: key);

  @override
  _DiaryWidgetState createState() => _DiaryWidgetState();
}

class _DiaryWidgetState extends State<DiaryWidget> {
  @override
  void initState() {
    super.initState();
    if (widget.onOpen != null) {
      widget.onOpen();
    }
  }

  @override
  void dispose() {
    if (widget.onClose != null) {
      widget.onClose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        title: Text(formatDayDate(widget.day?.date)),
      ),
      body: ListView.builder(
        itemCount: widget.day.notes.length,
        itemBuilder: (context, index) {
          var note = widget.day.notes[index];
          return _NoteCard(
            note,
            onTap: () {
              showAppBottomSheet(
                context: context,
                builder: (context) => _EditNote(note: note),
              ).then((result) {
                if (widget.onNoteUpdated != null && result != null) {
                  widget.onNoteUpdated(
                    UpdateNotePayload(
                      note: note,
                      newText: result,
                    ),
                  );
                }
              });
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showAppBottomSheet(
            context: context,
            builder: (context) => _EditNote(),
          ).then((result) {
            if (widget.onCreateNote != null && result != null) {
              widget.onCreateNote(result);
            }
          });
        },
        tooltip: AppStrings.newDiaryTooltip,
        child: Icon(Icons.add),
      ),
    );
  }
}

class _NoteCard extends StatelessWidget {
  final Note note;
  final VoidCallback onTap;

  const _NoteCard(this.note, {Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: onTap,
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(note.text),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      formatUpdatedAtDate(note.updatedAt),
                      style: TextStyle(
                        fontSize: 10.0,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _EditNote extends StatefulWidget {
  final Note note;

  const _EditNote({
    Key key,
    this.note = const Note(),
  }) : super(key: key);

  @override
  _EditNoteState createState() => _EditNoteState();
}

class _EditNoteState extends State<_EditNote> {
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();

    _controller.text = widget.note.text;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 360.0,
      padding: EdgeInsets.all(16.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: DropdownButton(
                  items: [
                    DropdownMenuItem(
                      child: Text('One'),
                    ),
                    DropdownMenuItem(
                      child: Text('Two'),
                    ),
                  ],
                  onChanged: (value) => null,
                ),
              ),
              const SizedBox(width: 16.0),
              FlatButton(
                onPressed: () {
                  Navigator.pop(
                    context,
                    _controller.text,
                  );
                },
                child: Text(AppStrings.save),
              ),
            ],
          ),
          const SizedBox(height: 16.0),
          SingleChildScrollView(
            child: TextFormField(
              controller: _controller,
              maxLines: null,
              decoration:
                  InputDecoration.collapsed(hintText: AppStrings.newNoteHint),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:diary/data/data.dart';
import 'package:diary/data/diary/diary.dart';
import 'package:flutter/material.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';

import 'widget.dart';

class DiaryPage extends StoreConnector<AppState, AppActions, DiaryState> {
  final DateTime date;

  DiaryPage(this.date);

  @override
  Widget build(BuildContext context, DiaryState state, AppActions actions) {
    return DiaryWidget(
      state.day,
      onOpen: () {
        actions.diaryActions.open(date);
      },
      onCreateNote: (text) {
        actions.diaryActions.createNote(text);
      },
      onNoteUpdated: (payload) {
        actions.diaryActions.updateNote(payload);
      },
      onClose: () {
        actions.diaryActions.close();
      },
    );
  }

  @override
  DiaryState connect(AppState state) {
    return state.diaryState;
  }
}

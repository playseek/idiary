import 'package:diary/data/diary/diary.dart';
import 'package:diary/utils/datetime.dart';
import 'package:diary/utils/navigation.dart';
import 'package:diary/utils/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel;
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';

class HomeWidget extends StatefulWidget {
  final List<Day> diaries;
  final bool reminderOn;
  final String reminderTime;
  final ValueChanged<bool> onReminderChanged;
  final ValueChanged<String> onReminderTimeChanged;
  final VoidCallback onOpen;

  const HomeWidget({
    Key key,
    this.diaries,
    this.onOpen,
    this.reminderOn = false,
    this.reminderTime = AppStrings.defaultReminder,
    this.onReminderChanged,
    this.onReminderTimeChanged,
  }) : super(key: key);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  @override
  void initState() {
    super.initState();
    if (widget.onOpen != null) {
      widget.onOpen();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppStrings.appName),
      ),
      body: ListView(children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 16.0),
          child: CalendarCarousel(
            height: 430.0,
            weekendTextStyle: TextStyle(
              color: Colors.red,
            ),
            thisMonthDayBorderColor: Colors.grey,
            weekFormat: false,
            daysHaveCircularBorder: false,
            daysTextStyle: TextStyle(color: Colors.white),
            onDayPressed: (date, list) {
              openDayPage(context, date);
            },
            markedDatesMap: EventList(
              events: Map.fromIterable(
                widget.diaries,
                key: (day) => day.date,
                value: (day) => day.notes,
              ),
            ),
            markedDateIconBorderColor: Colors.blue,
            markedDateIconMaxShown: 3,
            maxSelectedDate: DateTime.now(),
            inactiveDaysTextStyle: TextStyle(color: Colors.grey),
          ),
        ),
        ListTile(
          title: Text(AppStrings.remindAt(widget.reminderTime)),
          subtitle: Text(AppStrings.reminderDescription),
          trailing: Switch(
            value: widget.reminderOn,
            onChanged: widget.onReminderChanged,
          ),
          onTap: () {
            if (widget.onReminderChanged != null) {
              widget.onReminderChanged(!widget.reminderOn);
            }
          },
          onLongPress: () {
            showTimePicker(
              context: context,
              initialTime: TimeOfDay(
                hour: getHour(widget.reminderTime),
                minute: getMinute(widget.reminderTime),
              ),
            ).then((value) {
              if (widget.onReminderTimeChanged != null && value != null) {
                widget.onReminderTimeChanged(value.format(context));
              }
            });
          },
        ),
      ]),
    );
  }
}

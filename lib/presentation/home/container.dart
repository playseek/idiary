import 'package:diary/data/data.dart';
import 'package:diary/presentation/home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_built_redux/flutter_built_redux.dart';

class HomePage extends StoreConnector<AppState, AppActions, HomeState> {
  @override
  Widget build(BuildContext context, HomeState state, AppActions actions) {
    return HomeWidget(
      diaries: state.diaries,
      reminderOn: state.isReminderOn,
      reminderTime: state.reminderTime,
      onOpen: () {
        actions.homeActions.open();
      },
      onReminderChanged: (isReminderOn) {
        actions.homeActions.reminderChanged(isReminderOn);
      },
      onReminderTimeChanged: (time) {
        actions.homeActions.reminderTimeChanged(time);
      },
    );
  }

  @override
  HomeState connect(AppState state) {
    return state.homeState;
  }
}

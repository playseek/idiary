import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';

import 'diary/diary.dart';

class SharedPreferencesManager {
  Future<List<Day>> getDays() {
    return SharedPreferences.getInstance().then((_sharedPreferences) =>
        _sharedPreferences
            .getStringList(_Keys.DAYS)
            .map((dayKey) => Day.fromJson(
                jsonDecode(_sharedPreferences.getString(dayKey)) as Map))
            .toList());
  }

  Future<Day> getDay(DateTime date) {
    return SharedPreferences.getInstance()
        .then((_sharedPreferences) =>
            jsonDecode(_sharedPreferences.getString(_getDayKey(date))))
        .then((str) => Day.fromJson(str as Map))
        .catchError((e) => Day(date: date));
  }

  Future<Day> saveNote(DateTime date, String noteId, String newText) {
    return SharedPreferences.getInstance().then((_sharedPreferences) async {
      var dayKey = _getDayKey(date);

      Day day;

      try {
        day = Day.fromJson(
            jsonDecode(_sharedPreferences.getString(dayKey)) as Map);
      } catch (e) {
        day = Day(date: date);
      }
      var notes = List<Note>.from(day.notes);

      int index = notes.indexWhere((note) => note.id == noteId);

      if (index == -1) {
        notes.add(
          Note(
            id: Uuid().v1(),
            text: newText,
            updatedAt: DateTime.now(),
          ),
        );
      } else {
        Note note = notes[index];
        notes.removeAt(index);
        notes.add(
          Note(
            id: note.id,
            text: newText,
            updatedAt: DateTime.now(),
          ),
        );
      }

      notes.sort((one, two) => two.updatedAt.compareTo(one.updatedAt));

      var updatedDay = Day(
        id: day.id,
        date: day.date,
        notes: notes,
      );

      await _sharedPreferences.setString(
          dayKey, jsonEncode(Day.toMap(updatedDay)));

      List<String> days = _sharedPreferences.getStringList(_Keys.DAYS) ?? [];
      if (!days.contains(dayKey)) {
        days.add(dayKey);
        await _sharedPreferences.setStringList(_Keys.DAYS, days);
      }

      return updatedDay;
    });
  }

  Future<bool> isReminderOn() => SharedPreferences.getInstance()
      .then((_prefs) => _prefs.getBool(_Keys.REMINDER_ON));

  Future<bool> toggleReminder(bool isOn) =>
      SharedPreferences.getInstance().then((_prefs) async {
        await _prefs.setBool(_Keys.REMINDER_ON, isOn);
        return isOn;
      });

  Future<String> setReminderTime(String time) =>
      SharedPreferences.getInstance().then((_prefs) async {
        await _prefs.setString(_Keys.REMINDER_TIME, time);
        return time;
      });

  Future<String> getReminderTime() => SharedPreferences.getInstance()
      .then((_prefs) => _prefs.getString(_Keys.REMINDER_TIME));
}

abstract class _Keys {
  static const DAY = 'day_';
  static const DAYS = 'days';
  static const REMINDER_ON = 'reminder_is_on';
  static const REMINDER_TIME = 'reminder_time';
}

String _getDayKey(DateTime date) =>
    '${_Keys.DAY}${DateTime(date.year, date.month, date.day).toIso8601String()}';

import 'package:diary/data/data.dart';
import 'package:diary/utils/notification.dart';

import 'diary/diary.dart';

abstract class AppRepository {
  Future<List<Day>> getDays();

  Future<Day> getDay(DateTime date);

  Future<Day> updateNote(DateTime date, String noteId, String text);

  Future<bool> setReminderOn(bool isReminderOn);

  Future<bool> isReminderOn();

  Future<String> setReminderTime(String time);

  Future<String> getReminderTime();
}

abstract class AppLocalDataSource {
  Future<List<Day>> getDays();

  Future<Day> getDay(DateTime date);

  Future<Day> updateNote(DateTime date, String noteId, String text);

  Future<bool> setReminderOn(bool isReminderOn);

  Future<bool> isReminderOn();

  Future<String> setReminderTime(String time);

  Future<String> getReminderTime();
}

class RealAppRepository implements AppRepository {
  final AppLocalDataSource localDataSource;
  final NotificationManager notificationManager;

  const RealAppRepository(this.localDataSource, {this.notificationManager});

  @override
  Future<Day> getDay(DateTime date) {
    return localDataSource.getDay(date);
  }

  @override
  Future<Day> updateNote(DateTime date, String noteId, String text) {
    return localDataSource.updateNote(date, noteId, text);
  }

  @override
  Future<List<Day>> getDays() {
    return localDataSource.getDays();
  }

  @override
  Future<bool> setReminderOn(bool isReminderOn) {
    return Future(() {
      localDataSource.getReminderTime().then((time) {
        if (isReminderOn) {
          notificationManager?.scheduleReminder(time);
        } else {
          notificationManager?.cancelReminder();
        }
      });
      return localDataSource.setReminderOn(isReminderOn);
    });
  }

  @override
  Future<String> setReminderTime(String time) {
    return Future(() {
      localDataSource.isReminderOn().then((isOn) {
        if (isOn) {
          notificationManager?.cancelReminder();
          notificationManager?.scheduleReminder(time);
        }
      });
      return localDataSource.setReminderTime(time);
    });
  }

  @override
  Future<bool> isReminderOn() {
    return localDataSource.isReminderOn();
  }

  @override
  Future<String> getReminderTime() {
    return localDataSource.getReminderTime();
  }
}

class RealAppLocalDataSource implements AppLocalDataSource {
  final SharedPreferencesManager preferencesManager;

  const RealAppLocalDataSource(this.preferencesManager);

  @override
  Future<Day> getDay(DateTime date) {
    return preferencesManager.getDay(date);
  }

  @override
  Future<Day> updateNote(DateTime date, String noteId, String text) {
    return preferencesManager
        .getDay(date)
        .then((day) => preferencesManager.saveNote(date, noteId, text));
  }

  @override
  Future<List<Day>> getDays() {
    return preferencesManager.getDays();
  }

  @override
  Future<bool> setReminderOn(bool isReminderOn) {
    return preferencesManager.toggleReminder(isReminderOn);
  }

  @override
  Future<String> setReminderTime(String time) {
    return preferencesManager.setReminderTime(time);
  }

  @override
  Future<bool> isReminderOn() {
    return preferencesManager.isReminderOn();
  }

  @override
  Future<String> getReminderTime() {
    return preferencesManager.getReminderTime();
  }
}

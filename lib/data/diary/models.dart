part of diary;

class Day {
  final String id;
  final DateTime date;
  final List<Note> notes;

  const Day({
    this.id = "",
    this.date,
    this.notes = const [],
  });

  factory Day.fromJson(Map<String, dynamic> parsedJson) => Day(
        id: parsedJson['id'],
        date: DateTime.parse(parsedJson['date']),
        notes: (parsedJson['notes'] as Iterable)
                ?.map((v) => Note.fromJson(v as Map))
                ?.toList() ??
            [],
      );

  static Map<String, dynamic> toMap(Day day) => {
        'id': day.id,
        'date': day.date?.toIso8601String(),
        'notes': day.notes?.map((note) => Note.toMap(note))?.toList(),
      };

  static String toJson(Day day) => toMap(day).toString();
}

class Note {
  final String id;
  final String title;
  final String text;
  final DateTime updatedAt;

  const Note({
    this.id = "",
    this.title = "",
    this.text = "",
    this.updatedAt,
  });

  factory Note.fromJson(Map<String, dynamic> parsedJson) => Note(
        id: parsedJson['id'],
        title: parsedJson['title'],
        text: parsedJson['text'],
        updatedAt: DateTime.parse(parsedJson['updatedAt']),
      );

  static Map<String, dynamic> toMap(Note note) => {
        'id': note.id,
        'title': note.title,
        'text': note.text,
        'updatedAt': note.updatedAt?.toIso8601String(),
      };

  static String toJson(Note note) => toMap(note).toString();
}

class UpdateNotePayload {
  final Note note;
  final String newText;

  const UpdateNotePayload({
    this.note,
    this.newText = "",
  });
}

part of diary;

abstract class DiaryState implements Built<DiaryState, DiaryStateBuilder> {
  static Serializer<DiaryState> get serializer => _$diaryStateSerializer;

  DiaryState._();

  factory DiaryState([updates(HomeStateBuilder b)]) =>
      _$DiaryState((b) => b..day = Day());

  Day get day;
}

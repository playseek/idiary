part of diary;

NestedMiddlewareBuilder<
    AppState,
    AppStateBuilder,
    AppActions,
    DiaryState,
    DiaryStateBuilder,
    DiaryActions> createDiaryMiddleware(AppRepository repository) {
  return (NestedMiddlewareBuilder<
      AppState,
      AppStateBuilder,
      AppActions,
      DiaryState,
      DiaryStateBuilder,
      DiaryActions>((s) => s.diaryState, (a) => a.diaryActions)
    ..add(DiaryActionsNames.open, _loadDay(repository))
    ..add(DiaryActionsNames.updateNote, _updateNote(repository))
    ..add(DiaryActionsNames.createNote, _createNote(repository)));
}

MiddlewareHandler<DiaryState, DiaryStateBuilder, DiaryActions, DateTime>
    _loadDay(AppRepository repository) {
  return (MiddlewareApi<DiaryState, DiaryStateBuilder, DiaryActions> api,
      ActionHandler next, Action<DateTime> action) {
    repository.getDay(action.payload).then((day) => api.actions.showDay(day));

    next(action);
  };
}

MiddlewareHandler<DiaryState, DiaryStateBuilder, DiaryActions,
    UpdateNotePayload> _updateNote(AppRepository repository) {
  return (MiddlewareApi<DiaryState, DiaryStateBuilder, DiaryActions> api,
      ActionHandler next, Action<UpdateNotePayload> action) {
    repository
        .updateNote(
            api.state.day.date, action.payload.note.id, action.payload.newText)
        .then((day) => api.actions.showDay(day));

    next(action);
  };
}

MiddlewareHandler<DiaryState, DiaryStateBuilder, DiaryActions, String>
    _createNote(AppRepository repository) {
  return (MiddlewareApi<DiaryState, DiaryStateBuilder, DiaryActions> api,
      ActionHandler next, Action<String> action) {
    repository
        .updateNote(api.state.day.date, "", action.payload)
        .then((day) => api.actions.showDay(day));

    next(action);
  };
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of diary;

// **************************************************************************
// BuiltReduxGenerator
// **************************************************************************

// ignore_for_file: avoid_classes_with_only_static_members
// ignore_for_file: annotate_overrides

class _$DiaryActions extends DiaryActions {
  factory _$DiaryActions() => new _$DiaryActions._();
  _$DiaryActions._() : super._();

  final ActionDispatcher<DateTime> open =
      new ActionDispatcher<DateTime>('DiaryActions-open');
  final ActionDispatcher<Null> close =
      new ActionDispatcher<Null>('DiaryActions-close');
  final ActionDispatcher<Day> showDay =
      new ActionDispatcher<Day>('DiaryActions-showDay');
  final ActionDispatcher<UpdateNotePayload> updateNote =
      new ActionDispatcher<UpdateNotePayload>('DiaryActions-updateNote');
  final ActionDispatcher<String> createNote =
      new ActionDispatcher<String>('DiaryActions-createNote');

  @override
  void setDispatcher(Dispatcher dispatcher) {
    open.setDispatcher(dispatcher);
    close.setDispatcher(dispatcher);
    showDay.setDispatcher(dispatcher);
    updateNote.setDispatcher(dispatcher);
    createNote.setDispatcher(dispatcher);
  }
}

class DiaryActionsNames {
  static final ActionName<DateTime> open =
      new ActionName<DateTime>('DiaryActions-open');
  static final ActionName<Null> close =
      new ActionName<Null>('DiaryActions-close');
  static final ActionName<Day> showDay =
      new ActionName<Day>('DiaryActions-showDay');
  static final ActionName<UpdateNotePayload> updateNote =
      new ActionName<UpdateNotePayload>('DiaryActions-updateNote');
  static final ActionName<String> createNote =
      new ActionName<String>('DiaryActions-createNote');
}

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DiaryState> _$diaryStateSerializer = new _$DiaryStateSerializer();

class _$DiaryStateSerializer implements StructuredSerializer<DiaryState> {
  @override
  final Iterable<Type> types = const [DiaryState, _$DiaryState];
  @override
  final String wireName = 'DiaryState';

  @override
  Iterable serialize(Serializers serializers, DiaryState object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'day',
      serializers.serialize(object.day, specifiedType: const FullType(Day)),
    ];

    return result;
  }

  @override
  DiaryState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DiaryStateBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'day':
          result.day = serializers.deserialize(value,
              specifiedType: const FullType(Day)) as Day;
          break;
      }
    }

    return result.build();
  }
}

class _$DiaryState extends DiaryState {
  @override
  final Day day;

  factory _$DiaryState([void Function(DiaryStateBuilder) updates]) =>
      (new DiaryStateBuilder()..update(updates)).build();

  _$DiaryState._({this.day}) : super._() {
    if (day == null) {
      throw new BuiltValueNullFieldError('DiaryState', 'day');
    }
  }

  @override
  DiaryState rebuild(void Function(DiaryStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DiaryStateBuilder toBuilder() => new DiaryStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DiaryState && day == other.day;
  }

  @override
  int get hashCode {
    return $jf($jc(0, day.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DiaryState')..add('day', day))
        .toString();
  }
}

class DiaryStateBuilder implements Builder<DiaryState, DiaryStateBuilder> {
  _$DiaryState _$v;

  Day _day;
  Day get day => _$this._day;
  set day(Day day) => _$this._day = day;

  DiaryStateBuilder();

  DiaryStateBuilder get _$this {
    if (_$v != null) {
      _day = _$v.day;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DiaryState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DiaryState;
  }

  @override
  void update(void Function(DiaryStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DiaryState build() {
    final _$result = _$v ?? new _$DiaryState._(day: day);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new

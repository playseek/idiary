part of diary;

var diaryReducerBuilder = NestedReducerBuilder<AppState, AppStateBuilder,
    DiaryState, DiaryStateBuilder>((s) => s.diaryState, (b) => b.diaryState)
  ..add(DiaryActionsNames.showDay, _showDay);

void _showDay(DiaryState state, Action<Day> action, DiaryStateBuilder builder) {
  builder.update((b) => b..day = action.payload);
}

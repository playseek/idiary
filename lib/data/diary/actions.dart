part of diary;

abstract class DiaryActions extends ReduxActions {
  DiaryActions._();

  factory DiaryActions() => _$DiaryActions();

  ActionDispatcher<DateTime> get open;

  ActionDispatcher<Null> get close;

  ActionDispatcher<Day> get showDay;

  ActionDispatcher<UpdateNotePayload> get updateNote;

  ActionDispatcher<String> get createNote;
}

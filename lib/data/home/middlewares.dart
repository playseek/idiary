part of home;

NestedMiddlewareBuilder<
    AppState,
    AppStateBuilder,
    AppActions,
    HomeState,
    HomeStateBuilder,
    HomeActions> createHomeMiddleware(AppRepository repository) {
  return (NestedMiddlewareBuilder<
      AppState,
      AppStateBuilder,
      AppActions,
      HomeState,
      HomeStateBuilder,
      HomeActions>((s) => s.homeState, (a) => a.homeActions)
    ..add(HomeActionsNames.open, _loadDays(repository))
    ..add(HomeActionsNames.reminderChanged, _toggleReminder(repository))
    ..add(
        HomeActionsNames.reminderTimeChanged, _changeReminderTime(repository)));
}

MiddlewareHandler<HomeState, HomeStateBuilder, HomeActions, Null> _loadDays(
    AppRepository repository) {
  return (MiddlewareApi<HomeState, HomeStateBuilder, HomeActions> api,
      ActionHandler next, Action<Null> action) {
    repository.getDays().then((days) => api.actions.showDays(days));
    repository
        .isReminderOn()
        .then((isOn) => api.actions.reminderChangeSuccess(isOn));
    repository
        .getReminderTime()
        .then((time) => api.actions.reminderTimeChangeSuccess(time));
    next(action);
  };
}

MiddlewareHandler<HomeState, HomeStateBuilder, HomeActions, bool>
    _toggleReminder(AppRepository repository) {
  return (MiddlewareApi<HomeState, HomeStateBuilder, HomeActions> api,
      ActionHandler next, Action<bool> action) {
    repository
        .setReminderOn(action.payload)
        .then((isOn) => api.actions.reminderChangeSuccess(isOn));
    next(action);
  };
}

MiddlewareHandler<HomeState, HomeStateBuilder, HomeActions, String>
    _changeReminderTime(AppRepository repository) {
  return (MiddlewareApi<HomeState, HomeStateBuilder, HomeActions> api,
      ActionHandler next, Action<String> action) {
    repository
        .setReminderTime(action.payload)
        .then((time) => api.actions.reminderTimeChangeSuccess(time));
    next(action);
  };
}

part of home;

var homeReducerBuilder = NestedReducerBuilder<AppState, AppStateBuilder,
    HomeState, HomeStateBuilder>((s) => s.homeState, (b) => b.homeState)
  ..add(HomeActionsNames.showDays, _showDays)
  ..add(HomeActionsNames.updateDay, _showDay)
  ..add(HomeActionsNames.reminderChangeSuccess, _changeReminder)
  ..add(HomeActionsNames.reminderTimeChangeSuccess, _changeTime);

void _showDays(
    HomeState state, Action<List<Day>> action, HomeStateBuilder builder) {
  builder.update((b) => b..diaries = action.payload);
}

void _showDay(HomeState state, Action<Day> action, HomeStateBuilder builder) {
  var index =
      state.diaries.indexWhere((day) => day.date == action.payload.date);
  if (index > -1) {
    var newList = List<Day>.from(state.diaries);
    newList[index] = action.payload;
    builder..update((b) => b..diaries = newList);
  } else {
    var newList = List<Day>.from(state.diaries);
    newList.add(action.payload);
    builder..update((b) => b..diaries = newList);
  }
}

void _changeReminder(
    HomeState state, Action<bool> action, HomeStateBuilder builder) {
  builder.update((b) => b..isReminderOn = action.payload);
}

void _changeTime(
    HomeState state, Action<String> action, HomeStateBuilder builder) {
  builder.update((b) => b..reminderTime = action.payload);
}

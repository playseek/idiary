part of home;

abstract class HomeState implements Built<HomeState, HomeStateBuilder> {
  static Serializer<HomeState> get serializer => _$homeStateSerializer;

  HomeState._();

  factory HomeState([updates(HomeStateBuilder b)]) => _$HomeState((b) => b
    ..diaries = []
    ..isReminderOn = false
    ..reminderTime = AppStrings.defaultReminder);

  List<Day> get diaries;

  bool get isReminderOn;

  String get reminderTime;
}

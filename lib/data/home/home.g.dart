// GENERATED CODE - DO NOT MODIFY BY HAND

part of home;

// **************************************************************************
// BuiltReduxGenerator
// **************************************************************************

// ignore_for_file: avoid_classes_with_only_static_members
// ignore_for_file: annotate_overrides

class _$HomeActions extends HomeActions {
  factory _$HomeActions() => new _$HomeActions._();
  _$HomeActions._() : super._();

  final ActionDispatcher<Null> open =
      new ActionDispatcher<Null>('HomeActions-open');
  final ActionDispatcher<List<Day>> showDays =
      new ActionDispatcher<List<Day>>('HomeActions-showDays');
  final ActionDispatcher<Day> updateDay =
      new ActionDispatcher<Day>('HomeActions-updateDay');
  final ActionDispatcher<bool> reminderChanged =
      new ActionDispatcher<bool>('HomeActions-reminderChanged');
  final ActionDispatcher<bool> reminderChangeSuccess =
      new ActionDispatcher<bool>('HomeActions-reminderChangeSuccess');
  final ActionDispatcher<String> reminderTimeChanged =
      new ActionDispatcher<String>('HomeActions-reminderTimeChanged');
  final ActionDispatcher<String> reminderTimeChangeSuccess =
      new ActionDispatcher<String>('HomeActions-reminderTimeChangeSuccess');
  final ActionDispatcher<Null> close =
      new ActionDispatcher<Null>('HomeActions-close');

  @override
  void setDispatcher(Dispatcher dispatcher) {
    open.setDispatcher(dispatcher);
    showDays.setDispatcher(dispatcher);
    updateDay.setDispatcher(dispatcher);
    reminderChanged.setDispatcher(dispatcher);
    reminderChangeSuccess.setDispatcher(dispatcher);
    reminderTimeChanged.setDispatcher(dispatcher);
    reminderTimeChangeSuccess.setDispatcher(dispatcher);
    close.setDispatcher(dispatcher);
  }
}

class HomeActionsNames {
  static final ActionName<Null> open = new ActionName<Null>('HomeActions-open');
  static final ActionName<List<Day>> showDays =
      new ActionName<List<Day>>('HomeActions-showDays');
  static final ActionName<Day> updateDay =
      new ActionName<Day>('HomeActions-updateDay');
  static final ActionName<bool> reminderChanged =
      new ActionName<bool>('HomeActions-reminderChanged');
  static final ActionName<bool> reminderChangeSuccess =
      new ActionName<bool>('HomeActions-reminderChangeSuccess');
  static final ActionName<String> reminderTimeChanged =
      new ActionName<String>('HomeActions-reminderTimeChanged');
  static final ActionName<String> reminderTimeChangeSuccess =
      new ActionName<String>('HomeActions-reminderTimeChangeSuccess');
  static final ActionName<Null> close =
      new ActionName<Null>('HomeActions-close');
}

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<HomeState> _$homeStateSerializer = new _$HomeStateSerializer();

class _$HomeStateSerializer implements StructuredSerializer<HomeState> {
  @override
  final Iterable<Type> types = const [HomeState, _$HomeState];
  @override
  final String wireName = 'HomeState';

  @override
  Iterable serialize(Serializers serializers, HomeState object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'diaries',
      serializers.serialize(object.diaries,
          specifiedType: const FullType(List, const [const FullType(Day)])),
      'isReminderOn',
      serializers.serialize(object.isReminderOn,
          specifiedType: const FullType(bool)),
      'reminderTime',
      serializers.serialize(object.reminderTime,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  HomeState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new HomeStateBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'diaries':
          result.diaries = serializers.deserialize(value,
                  specifiedType:
                      const FullType(List, const [const FullType(Day)]))
              as List<Day>;
          break;
        case 'isReminderOn':
          result.isReminderOn = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'reminderTime':
          result.reminderTime = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$HomeState extends HomeState {
  @override
  final List<Day> diaries;
  @override
  final bool isReminderOn;
  @override
  final String reminderTime;

  factory _$HomeState([void Function(HomeStateBuilder) updates]) =>
      (new HomeStateBuilder()..update(updates)).build();

  _$HomeState._({this.diaries, this.isReminderOn, this.reminderTime})
      : super._() {
    if (diaries == null) {
      throw new BuiltValueNullFieldError('HomeState', 'diaries');
    }
    if (isReminderOn == null) {
      throw new BuiltValueNullFieldError('HomeState', 'isReminderOn');
    }
    if (reminderTime == null) {
      throw new BuiltValueNullFieldError('HomeState', 'reminderTime');
    }
  }

  @override
  HomeState rebuild(void Function(HomeStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HomeStateBuilder toBuilder() => new HomeStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HomeState &&
        diaries == other.diaries &&
        isReminderOn == other.isReminderOn &&
        reminderTime == other.reminderTime;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, diaries.hashCode), isReminderOn.hashCode),
        reminderTime.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('HomeState')
          ..add('diaries', diaries)
          ..add('isReminderOn', isReminderOn)
          ..add('reminderTime', reminderTime))
        .toString();
  }
}

class HomeStateBuilder implements Builder<HomeState, HomeStateBuilder> {
  _$HomeState _$v;

  List<Day> _diaries;
  List<Day> get diaries => _$this._diaries;
  set diaries(List<Day> diaries) => _$this._diaries = diaries;

  bool _isReminderOn;
  bool get isReminderOn => _$this._isReminderOn;
  set isReminderOn(bool isReminderOn) => _$this._isReminderOn = isReminderOn;

  String _reminderTime;
  String get reminderTime => _$this._reminderTime;
  set reminderTime(String reminderTime) => _$this._reminderTime = reminderTime;

  HomeStateBuilder();

  HomeStateBuilder get _$this {
    if (_$v != null) {
      _diaries = _$v.diaries;
      _isReminderOn = _$v.isReminderOn;
      _reminderTime = _$v.reminderTime;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(HomeState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$HomeState;
  }

  @override
  void update(void Function(HomeStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$HomeState build() {
    final _$result = _$v ??
        new _$HomeState._(
            diaries: diaries,
            isReminderOn: isReminderOn,
            reminderTime: reminderTime);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new

part of home;

abstract class HomeActions extends ReduxActions {
  HomeActions._();

  factory HomeActions() => _$HomeActions();

  ActionDispatcher<Null> get open;

  ActionDispatcher<List<Day>> get showDays;

  ActionDispatcher<Day> get updateDay;

  ActionDispatcher<bool> get reminderChanged;

  ActionDispatcher<bool> get reminderChangeSuccess;

  ActionDispatcher<String> get reminderTimeChanged;

  ActionDispatcher<String> get reminderTimeChangeSuccess;

  ActionDispatcher<Null> get close;
}

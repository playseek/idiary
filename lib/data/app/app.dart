library app;

import 'package:built_redux/built_redux.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:diary/data/diary/diary.dart';
import '../data.dart';

part 'actions.dart';
part 'app.g.dart';
part 'middlewares.dart';
part 'models.dart';
part 'reducers.dart';
part 'states.dart';

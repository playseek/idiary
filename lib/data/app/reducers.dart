part of app;

var appReducerBuilder = ReducerBuilder<AppState, AppStateBuilder>()
  ..combineNested(homeReducerBuilder)
  ..combineNested(diaryReducerBuilder);

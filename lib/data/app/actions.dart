part of app;

abstract class AppActions extends ReduxActions {
  AppActions._();

  factory AppActions() => _$AppActions();

  HomeActions homeActions;

  DiaryActions diaryActions;
}

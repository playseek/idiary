part of app;

Middleware<AppState, AppStateBuilder, AppActions> createAppMiddleware(
  AppRepository repository,
) {
  return (MiddlewareBuilder<AppState, AppStateBuilder, AppActions>()
        ..add(DiaryActionsNames.showDay, _diaryUpdated(repository))
        ..combineNested(createHomeMiddleware(repository))
        ..combineNested(createDiaryMiddleware(repository)))
      .build();
}

MiddlewareHandler<AppState, AppStateBuilder, AppActions, Day> _diaryUpdated(
    AppRepository repository) {
  return (MiddlewareApi<AppState, AppStateBuilder, AppActions> api,
      ActionHandler next, Action<Day> action) {
    api.actions.homeActions.updateDay(action.payload);
    next(action);
  };
}

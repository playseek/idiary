part of app;

abstract class AppState implements Built<AppState, AppStateBuilder> {
  static Serializer<AppState> get serializer => _$appStateSerializer;

  AppState._();

  factory AppState([updates(AppStateBuilder b)]) => _$AppState((b) =>
      b..homeState.replace(HomeState())..diaryState.replace(DiaryState()));

  HomeState get homeState;

  DiaryState get diaryState;
}

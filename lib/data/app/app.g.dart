// GENERATED CODE - DO NOT MODIFY BY HAND

part of app;

// **************************************************************************
// BuiltReduxGenerator
// **************************************************************************

// ignore_for_file: avoid_classes_with_only_static_members
// ignore_for_file: annotate_overrides

class _$AppActions extends AppActions {
  factory _$AppActions() => new _$AppActions._();
  _$AppActions._() : super._();

  final HomeActions homeActions = new HomeActions();
  final DiaryActions diaryActions = new DiaryActions();

  @override
  void setDispatcher(Dispatcher dispatcher) {
    homeActions.setDispatcher(dispatcher);
    diaryActions.setDispatcher(dispatcher);
  }
}

class AppActionsNames {}

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AppState> _$appStateSerializer = new _$AppStateSerializer();

class _$AppStateSerializer implements StructuredSerializer<AppState> {
  @override
  final Iterable<Type> types = const [AppState, _$AppState];
  @override
  final String wireName = 'AppState';

  @override
  Iterable serialize(Serializers serializers, AppState object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'homeState',
      serializers.serialize(object.homeState,
          specifiedType: const FullType(HomeState)),
      'diaryState',
      serializers.serialize(object.diaryState,
          specifiedType: const FullType(DiaryState)),
    ];

    return result;
  }

  @override
  AppState deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AppStateBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'homeState':
          result.homeState.replace(serializers.deserialize(value,
              specifiedType: const FullType(HomeState)) as HomeState);
          break;
        case 'diaryState':
          result.diaryState.replace(serializers.deserialize(value,
              specifiedType: const FullType(DiaryState)) as DiaryState);
          break;
      }
    }

    return result.build();
  }
}

class _$AppState extends AppState {
  @override
  final HomeState homeState;
  @override
  final DiaryState diaryState;

  factory _$AppState([void Function(AppStateBuilder) updates]) =>
      (new AppStateBuilder()..update(updates)).build();

  _$AppState._({this.homeState, this.diaryState}) : super._() {
    if (homeState == null) {
      throw new BuiltValueNullFieldError('AppState', 'homeState');
    }
    if (diaryState == null) {
      throw new BuiltValueNullFieldError('AppState', 'diaryState');
    }
  }

  @override
  AppState rebuild(void Function(AppStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AppStateBuilder toBuilder() => new AppStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AppState &&
        homeState == other.homeState &&
        diaryState == other.diaryState;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, homeState.hashCode), diaryState.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AppState')
          ..add('homeState', homeState)
          ..add('diaryState', diaryState))
        .toString();
  }
}

class AppStateBuilder implements Builder<AppState, AppStateBuilder> {
  _$AppState _$v;

  HomeStateBuilder _homeState;
  HomeStateBuilder get homeState =>
      _$this._homeState ??= new HomeStateBuilder();
  set homeState(HomeStateBuilder homeState) => _$this._homeState = homeState;

  DiaryStateBuilder _diaryState;
  DiaryStateBuilder get diaryState =>
      _$this._diaryState ??= new DiaryStateBuilder();
  set diaryState(DiaryStateBuilder diaryState) =>
      _$this._diaryState = diaryState;

  AppStateBuilder();

  AppStateBuilder get _$this {
    if (_$v != null) {
      _homeState = _$v.homeState?.toBuilder();
      _diaryState = _$v.diaryState?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AppState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AppState;
  }

  @override
  void update(void Function(AppStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AppState build() {
    _$AppState _$result;
    try {
      _$result = _$v ??
          new _$AppState._(
              homeState: homeState.build(), diaryState: diaryState.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'homeState';
        homeState.build();
        _$failedField = 'diaryState';
        diaryState.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AppState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
